package com.example.vehiclesapplication.models

import java.io.Serializable

class Vehicle: Serializable {
    var vehicleId = 0
    var vrn = ""
    var country = ""
    var color = ""
    var type = ""
    var default = true

}