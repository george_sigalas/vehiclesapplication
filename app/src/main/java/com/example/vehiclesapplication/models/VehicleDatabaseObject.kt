package com.example.vehiclesapplication.models

class VehicleDatabaseObject(vehicleDatabaseObject: VehicleDatabaseObject?) {
    var count = 0
    lateinit var vehicles: MutableList<Vehicle>
    var currentPage = 0
    var nextPage = 0
    var totalPages = 0

    init {
        if (vehicleDatabaseObject != null) {
            this.count = vehicleDatabaseObject.count
            this.vehicles = vehicleDatabaseObject.vehicles
            this.currentPage = vehicleDatabaseObject.currentPage
            this.nextPage = vehicleDatabaseObject.nextPage
            this.totalPages = vehicleDatabaseObject.totalPages
        }
    }

}