package com.example.vehiclesapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.vehiclesapplication.models.VehicleDatabaseObject
import com.example.vehiclesapplication.services.FetchVehicles
import java.lang.Exception

/**
 * A fragment representing a list of Vehicles.
 */
class VehicleList : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_item_list)
        try {
            var fetchedVehicles: VehicleDatabaseObject = FetchVehicles().vehiclesObject
            val recyclerView = findViewById<RecyclerView>(R.id.list)
            recyclerView.adapter = VehicleAdapter(fetchedVehicles?.vehicles)

        } catch (Error: Exception) {
            println(Error)
        }

    }

}