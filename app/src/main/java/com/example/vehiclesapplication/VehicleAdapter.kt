package com.example.vehiclesapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.vehiclesapplication.models.Vehicle


class VehicleAdapter(
    private val vehicles: MutableList<Vehicle>?
) : RecyclerView.Adapter<VehicleAdapter.VehicleViewHolder>() {

    class VehicleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val vehicleInformation: TextView = view.findViewById(R.id.vehicle_information)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VehicleViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_item, parent, false)
        return VehicleViewHolder(view)
    }

    override fun onBindViewHolder(holder: VehicleViewHolder, position: Int) {
        val item = vehicles?.get(position)
        val content = StringBuilder().append("Vehicle Reg Number: ${item?.vrn}")
        content.append("Vehicle Color: ${item?.color}")
        content.append("Vehicle Type: ${item?.type}")
        content.append("Vehicle Country: ${item?.country}")
        content.append("Is Default: ${item?.default}")
        holder.vehicleInformation.text = content.toString()
    }

    override fun getItemCount(): Int {
        return vehicles?.size ?: 0
    }

}