package com.example.vehiclesapplication.services

import java.net.URL

import com.google.gson.Gson
import com.example.vehiclesapplication.models.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class FetchVehicles() {
    lateinit var vehiclesObject: VehicleDatabaseObject

    init {
        getVehicles()

    }

    private fun getVehicles() {
        runBlocking {
            launch (context = Dispatchers.IO) {
                val gson = Gson()
                val url = URL("http://private-6d86b9-vehicles5.apiary-mock.com/vehicles")
                val jsonString = url.readText()
                val json = gson.fromJson(jsonString, VehicleDatabaseObject::class.java)
                this@FetchVehicles.vehiclesObject = json
                println(json.vehicles[0].color)
                return@launch
            }
        }
    }
}
